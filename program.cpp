// program.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <conio.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

const int n_matrix = 50;
const int m_matrix = 50;
const int array_size = 50;
const char path_array[] = "e:\\�����\\����������������\\�������\\input_array.txt";
const char path_matrix[] = "e:\\�����\\����������������\\�������\\input_matrix.txt";
const char path_matrix2[] = "e:\\�����\\����������������\\�������\\input_matrix2.txt";
char ch[array_size];
/*���������, ������� ������� � ������� �� ������� �����, �������� � ������������ ������,
����������� � �������� ���������� ��� ������� ���������� ������� � �� �����������*/
void math (int matrix1[m_matrix][n_matrix], int matrix2[m_matrix][n_matrix], int m, int n)
{
	cout << "����� ������:" << endl;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << (matrix1[i][j] + matrix2[i][j]) << ' ';
		}
		cout << endl;
	}
	cout << "�������� ������:" << endl;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << (matrix1[i][j] - matrix2[i][j]) << ' ';
		}
		cout << endl;
	}
	if (m == n)
	{
		cout << "������������ ������:" << endl;
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				int temp = 0;
				for (int l = 0; l < m; l++)
				{
					temp += matrix1[i][l] * matrix2[l][j];
				}
				cout << temp << ' ';
			}
			cout << endl;
		}
	}
	else
	{
		cout << "���������� ����������� ������� ��-�� �������������� �����������." << endl;
	}
}
/*���������, ����������� ���������� ������ � ��������� ����� �� ������ ��������� ���������.
����� ������������ ����� ��� �� �������*/
void array_of_eves (int array[], int n)
{
	int count = 0;
	int events[array_size];
	for (int i = 0; i < n; i++)
	{
		if (array[i] % 2 == 0)
		{
			events[count++] = array[i];
		}
	}
	cout << "������ ������ ���������:" << endl;
	for (int i = 0; i < count; i++)
	{
		cout << events[i] << ' ';
	}
	cout << endl;
}
/*���������, ����������� ���������� ������ �� �������� � ��������� ��� �� �������
� ������ �������, � ����� "� �����"*/
void sort (int array_for_sort[], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int max = array_for_sort[i];
		int imax = i;
		for (int j = i; j < n; j++)
		{
			if (array_for_sort[j] > max)
			{
				max = array_for_sort[j];
				imax = j;
			}
		}
		int temp = array_for_sort[i];
		array_for_sort[i] = array_for_sort[imax];
		array_for_sort[imax] = temp;
	}
	cout << "������, ��������������� �� ��������:" << endl;
	for (int i = 0; i < n; i++)
	{
		cout << array_for_sort[i] << ' ';
	}
	cout << endl;
	cout << "������, ��������������� �� �����������:" << endl;
	for (int i = n - 1; i >= 0; i--)
	{
		cout << array_for_sort[i] << ' ';
	}
	cout << endl;
}
/*������������� ��������� ������ � ������ �� �������
����. � ���. �������� � �� �������� � �������(������� ��� �����������)*/
void max_min (int array[array_size], int n)
{
	int imax = 0;
	int	imin = 0;
	int max = array[0];
	int min = array[0];
	for (int i = 1; i < n; i++)
	{
		if (array[i] > max)
		{
			max = array[i];
			imax = i;
		}
		if (array[i] < min)
		{
			min = array[i];
			imin = i;
		}
	}
	cout << "������������ ������� ������� = " << max << ", ��� ������ ����� "<< (imax + 1) << endl;
	cout << "����������� ������� ������� = " << min << ", ��� ������ ����� "<< (imin + 1) << endl;
}
/*������������� ��������� ������ � ������ �� �������
����. � ���. �������� � �� �������� � �������(������� ��� ����������)*/
void max_min (int matrix[m_matrix][n_matrix], int m, int n)
{
	int imax = 0;
	int imin = 0;
	int jmax = 0;
	int jmin = 0;
	int max = matrix[0][0];
	int min = matrix[0][0];
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (matrix[i][j] > max)
			{
				max = matrix[i][j];
				imax = i;
				jmax = j;
			}
			if (matrix[i][j] < min)
			{
				min = matrix[i][j];
				imin = i;
				jmin = j;
			}
		}
	}
	cout << "������������ ������� ������� = " << max << ", ��� ������ [" << (imax + 1) << "][" << (jmax + 1) << "]." << endl;
	cout << "����������� ������� ������� = " << min << ", ��� ������ [" << (imin + 1) << "][" << (jmin + 1) << "]." << endl;
}
//������� ��� ������ � ���������� ��������
void vector ()
{
	int choice, n, kod;
	bool flag = true;
	int array[array_size];
	while (flag)
	{
		system("cls");
		cout << "1 - ���� ������� � ����������.\n2 - ���� ������� �� �����." << endl;
		cin >> ch;
		if ((ch[0] >= '1') && (ch[0] <= '2'))
		{
			choice = ch[0]-'0';
			switch (choice)
			{
				//����� ������ ��������� � ���������� ��������, ������� �������� � ����������
				case 1:
					system("cls");
					cout << "������� ����������� �������\n";
					cin >> n;
					cout << "������� �������� �������:" << endl;
					for (int i = 0; i < n; )
					{
						int temp = scanf("%d", &array[i]);
						if (temp == 0)
						{
							cout << "������! ������� �������� ������� �����:" << endl;
							fflush (stdin);
							i = 0;
						}
						else
							i++;
					}
					fflush (stdin);
					while (flag)
					{
						system("cls");
						cout << "�������� ������:" << endl;
						for (int i = 0; i < n; i++)
							cout << array[i] << ' ';
						cout << endl;
						cout << "������� ��� ��������:\n1 - ����� max � min.\n2 - ������ � �������� ����������.\n3 - ������������ ������ ������� �� ������ ���������.\n0 - ����� � ����." << endl;
						cout << "���: ";
						cin >> ch;
						if ((ch[0] < '0') || (ch[0]>'9'))
						{
							continue;
						}
						else
						{
							kod = ch[0] - '0';
							switch (kod)
							{
								//����� ������ ����. � ���. ���������
								case 1:
									max_min(array, n);
									system("pause");
								break;
								//����� ��������� ���������� �������
								case 2:
									sort(array, n);
									system("pause");
								break;
								//����� ��������� ������������ ������ ������� �� ������ ���������
								case 3:
									array_of_eves(array, n);
									system("pause");
								break;
								//����� � ������� ���� ���������
								case 0:
									flag = false;
								break;
							}
						}
					}
				break;
				//����� ������ ��������� � ���������� ��������, ����������� �� �����
				case 2:
					system("cls");
					ifstream in;
					in.open(path_array);
					in >> n;
					cout << "�������� ������:" << endl;
					for (int i = 0; i < n; i++)
					{
						in >> array[i];
						cout << array[i] << ' ';
					}
					cout << endl;
					in.close();
					while (flag)
					{
						system("cls");
						cout << "�������� ������:" << endl;
						for (int i = 0; i < n; i++)
							cout << array[i] << ' ';
						cout << endl;
						cout << "������� ��� ��������:\n1 - ����� max � min.\n2 - ������ � �������� ����������.\n3 - ������������ ������ ������� �� ������ ���������.\n0 - ����� � ����." << endl;
						cout << "���:";
						cin >> ch;
						if ((ch[0] < '0') || (ch[0] > '9'))
							continue;
						else
						{
							kod = ch[0] - '0';
							switch (kod)
							{
								//����� ������ ����. � ���. ���������
								case 1:
									max_min(array, n);
									system("pause");
								break;
								case 2:
									//����� ��������� ���������� �������
									sort(array, n);
									system("pause");
								break;
								case 3:
									//����� ��������� ������������ ������ ������� �� ������ ���������
									array_of_eves(array, n);
									system("pause");
								break;
								//����� � ������� ���� ���������
								case 0:
									flag = false;
								break;
							}
						}
					}
				break;	
			}
		}
		else
		{
			continue;
		}
	}
}
//������� ��� ������ � ���������
void matrix ()
{
	int choice, n, m, kod;
	bool flag = true;
	int matrix[m_matrix][n_matrix];
	while (flag)
	{
		system("cls");
		cout << "1 - ���� ������� � ����������.\n2 - ���� ������� �� �����." << endl;
		cin >> ch;
		if ((ch[0] >= '1') && (ch[0] <= '2'))
		{
			choice = ch[0] - '0';
		switch (choice)
		{
			//����� ������ ��������� � ���������, ��������� � ����������
			case 1:
				system("cls");
				cout << "������� ����������� ������� ����� ������"<<endl;
				cin >> m >> n;
				cout << "������� �������� ������� ���������:" << endl;
				for (int i = 0; i < m; i++)
				{
					for (int j = 0; j < n; )
					{
						int temp = scanf("%d", &matrix[i][j]);
						if (temp == 0)
						{
							cout << "������! ������� �������� ������� �����:" << endl;
							fflush (stdin);
							j = 0;
						}
						else
							j++;
					}
				}
				fflush (stdin);
				while (flag)
				{
					system("cls");
					cout << "�������� �������:" << endl;
					for (int i = 0; i < m; i++)
					{
						for (int j = 0; j < n; j++)
						{
							cout << matrix[i][j] << ' ';
						}
						cout << endl;
					}
					cout << "������� ��� ��������:\n1 - ����� max � min.\n2 - �������������� �������� � ���������.\n0 - ����� � ����." << endl;
					cout << "���:";
					cin >> ch;
					if ((ch[0] < '0') || (ch[0]>'9'))
					{
						continue;
					}
					else
					{
						kod = ch[0] - '0';
						switch (kod)
						{
							case 1:
								//����� ��������� ������ ����. � ���. ���������
								max_min(matrix, m, n);
								system("pause");
							break;
							//���� ������ ������� � ����� ��������� �������������� �������� � ���������
							case 2:
								int matrix2[m_matrix][n_matrix];
								cout << "������� ������� �������� " << m << " �� " << n << endl;
								for (int i = 0; i < m; i++)
								{
									for (int j = 0; j < n; )
									{
										int temp = scanf("%d", &matrix2[i][j]);
										if (temp == 0)
									{
										cout << "������! ������� �������� ������� �����:" << endl;
										fflush (stdin);
										j = 0;
									}
									else
										j++;
									}
								}
								fflush (stdin);
								system("cls");
								cout << "�������� �������:" << endl;
								for (int i = 0; i < m; i++)
								{
									for (int j = 0; j < n; j++)
									{
										cout << matrix[i][j] << ' ';
									}
									cout << endl;
								}
								cout << "������ �������:" << endl;
								for (int i = 0; i < m; i++)
								{
									for (int j = 0; j < n; j++)
									{
										cout << matrix2[i][j] << ' ';
									}
									cout << endl;
								}
								math(matrix, matrix2, m, n);
								system("pause");
							break;
							//����� � ������� ���� ���������
							case 0:
								flag = false;
							break;
						}
					}
				}
			break;
			//����� ������ ��������� � ���������, ��������� �� �����
			case 2:
				system("cls");
				ifstream in;
				ifstream in2;
				in.open(path_matrix);
				in >> m >> n;
				cout << "�������� �������:" << endl;
				for (int i = 0; i < m; i++)
				{
					for (int j = 0; j < n; j++)
					{
						in >> matrix[i][j];
						cout << matrix[i][j] << ' ';
					}
					cout << endl;
				}
				in.close();
				while (flag)
				{
					system("cls");
					cout << "�������� �������:" << endl;
					for (int i = 0; i < m; i++)
					{
						for (int j = 0; j < n; j++)
						{
							cout << matrix[i][j] << ' ';
						}
						cout << endl;
					}
					cout << "������� ��� ��������:\n1 - ����� max � min.\n2 - �������������� �������� � ���������.\n0 - ����� � ����." << endl;
					cout << "���:";
					cin >> ch;
					if ((ch[0] < '0') || (ch[0] > '9'))
						continue;
					else
					{
						kod = ch[0] - '0';
						switch (kod)
						{
							//����� ��������� ������ ����. � ���. ���������
							case 1:
								max_min(matrix, m, n);
								system("pause");
							break;
							//������ ������ ������� �� ����� � ����� ��������� �������������� �������� � ���������
							case 2:
								int matrix2[m_matrix][n_matrix];
								in2.open(path_matrix2);
								in2 >> m >> n;
								cout << "������ �������:" << endl;
								for (int i = 0; i < m; i++)
								{
									for (int j = 0; j < n; j++)
									{
										in2 >> matrix2[i][j];
										cout << matrix2[i][j] << ' ';
									}
									cout << endl;
								}	
								in2.close();
								math(matrix, matrix2, m, n);
								system("pause");
							break;
							//����� � ������� ���� ���������
							case 0:
								flag = false;
							break;
						}
					}
				}
			break;
			}
		}
	}
}
//������� ���� ���������
int main()
{
	setlocale(LC_ALL, "Russian");
	int uprav;
	bool flag = true;
	while (flag)
	{	
		system("cls");
		cout << "������� 1 ��� ������ � ����������� ���������.\n������� 2 ��� ������ � ���������.\n������� 0 ��� ������ �� ���������." << endl;
		cin >> ch;
		if ((ch[0] < '0') || (ch[0] > '2'))
		{
			continue;
		}
		else
		{
			uprav = ch[0] - '0';
			switch (uprav)
			{
				case 1:
					vector();
				break;
				case 2:
					matrix();
				break;
				case 0: 
					flag = false;
				break;
			}
		}
	}
	return 0;
}